

--

For example, it is easy to replace xargs to gzip all html files in the current directory and its subdirectories:
```shell
find . -type f -name '*.html' -print | parallel gzip
```

Functions are declared using this syntax:

```
fname () compound-command [ redirections ]
```
or
```shell
function fname [()] compound-command [ redirections ]
```

When calling functions, there are two ways to pass parameters into them, either by including
them in the function signature or simply displaying the value in front of the function call.
#Included in method signature.
```
[string]::Equals("Rui","Machado")
```
#Using PowerShell front function parameters.
```
MyFunction -param1 65 -param2 3
```