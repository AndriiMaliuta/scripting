<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <PersonList>
            <xsl:for-each select="PersonList/Person">
                <Person>
                    <Data>
                        <xsl:value-of select="Name" />;
                        <xsl:value-of select="Age" />;
                        <xsl:value-of select="Address" />
                    </Data>
                </Person>
            </xsl:for-each>
        </PersonList>
    </xsl:template>
</xsl:stylesheet>