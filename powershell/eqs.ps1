$a="Hello"
$b="Hi"
#EQUALS Returns False
($a -eq $b)
#NOT EQUALS Returns True
($a -ne $b)
#GREATER THAN OR EQUAL Returns True
(10 -ge 10)
#GREATER THAN Returns False
(10 -gt 10)
#LESS THAN Returns True
(3 -lt 7)
#GREATER THAN Returns True
(3 -le 7)
#LIKE Returns true
($a -like "H*")
#LIKE Returns false
($a -like "H?")
#MATCH Returns true
($a -match "(.*)")
#CONTAINS Returns true
(1,2,3,4,5 -contains 5)
#CONTAINS Returns False
(1,2,3,4,5 -contains 15)
#IS Returns true
($a -is [System.String])

#AND Return False
($a -ne $b) -and (1 -eq 1) -and ($a -eq "PowerShell")
#OR Return True
($a -ne $b) -or (1 -eq 1) -or ($a -eq "PowerShell")
#XOR Returns True
($a -eq $b) -xor ($a -like "H*")
#NOT Returns False
-not ($a -ne $b)
#Combining multiple operators - Returns False
(($a -eq $b) -xor ($a -like "H*") -and (1,2,3,4 -contains 1)) -xor ("A" -ne "B")
