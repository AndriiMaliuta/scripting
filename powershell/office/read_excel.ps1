#Load Excel PIAs assembly.
[Reflection.Assembly]::LoadWithPartialname("Microsoft.Office.Interop.Excel")
#Kill all Excel processes.
Get-Process | where{$_.ProcessName -like "*EXCEL*"} | kill
#Instantiate a new Excel application
$excel = new-object Microsoft.Office.Interop.Excel.ApplicationClass
#Path to our Excel file.
$filePath="c:\temp\test.xlsx"
#Instatiate a new workbook and then its path.
$book = $excel.Workbooks.Open("$filePath")
#Get the sheet - In this case we just have one.
$sheet = $book.Worksheets.Item(1)
#Get all list objects.
$lObjects = $sheet.ListObjects
#Get the range of data.
$range = $lObjects.Item(1).Range
#Print all cell data.
$lObjects.Item(1).Range.Rows | %{
    $row = $_.Row
    $lObjects.Item(1).Range.Columns | % {
        $col = $_.Column
        #Cell item
        write $range.Item($row,$col).Value2
    }
}