[Reflection.Assembly]::LoadWithPartialname("AssemblyName")
# ==================
#Load Microsoft Office Excel Assembly
[Reflection.Assembly]::LoadWithPartialname("Microsoft.Office.Interop.Excel")
#Load Microsoft Office Access Assembly
[Reflection.Assembly]::LoadWithPartialname("Microsoft.Office.Interop.Access")
#Load Microsoft Office InfoPath Assembly
[Reflection.Assembly]::LoadWithPartialname("Microsoft.Office.Interop.InfoPath")
#Load Microsoft Office OneNote Assembly
[Reflection.Assembly]::LoadWithPartialname("Microsoft.Office.Interop.OneNote")
#Load Microsoft Office Outlook Assembly
[Reflection.Assembly]::LoadWithPartialname("Microsoft.Office.Interop.Outlook")
#Load Microsoft Office PowerPoint Assembly
[Reflection.Assembly]::LoadWithPartialname("Microsoft.Office.Interop.PowerPoint")
#Load Microsoft Office Publisher Assembly
[Reflection.Assembly]::LoadWithPartialname("Microsoft.Office.Interop.Publisher")
#Load Microsoft Office Visio Assembly
[Reflection.Assembly]::LoadWithPartialname("Microsoft.Office.Interop.Visio")
#Load Microsoft Office Word Assembly
[Reflection.Assembly]::LoadWithPartialname("Microsoft.Office.Interop.Word")