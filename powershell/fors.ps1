$array = 1,2,3,4,5,6
for($i=0;$i -lt $array.Count;$i++){
    write $array[$i]
}

#BASE
$array = 1,2,3,4,5,6
#Traditional way - classic foreach
foreach($var in $array){
    write $var
}
<# Using the most common way to
iterate objects in PowerShell
- input object foreach
#>
$array | %{
    write $_
}