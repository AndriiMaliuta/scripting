$pathToFolder = "c:\temp\newFolder"
#Get the ACL of the folder.
$acl = Get-Acl -Path $pathToFolder
#set the new permission settings.
$perSettings = "BUILTIN\Users","FullControl","Deny"
#create the access rule.
$newRule = New-Object System.Security.AccessControl.FileSystemAccessRule
$perSettings
#change the acl access rule.
$acl.SetAccessRule($newRule)
#Set the new rules in the folder ACL.
$acl | Set-Acl $pathToFolder