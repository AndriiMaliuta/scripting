$path = "c:\temp\"
Get-ChildItem -Path $path -Name "File.txt" | %{
    Rename-Item -Path "$path\$_" -NewName "FileRenamed.txt"
}
Rename-Item -Path "$path\FileRenamed.txt" -NewName "File.txt"