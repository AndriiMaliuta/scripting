#Get the content of a single file without restrictions.
Get-Content -Path "/home/andrii/prog/bash/cli-playg/test.txt"

Get-ChildItem -Path "c:\temp" -Filter "*.txt" | %{
    #Get Content
    Get-Content -Path $_.FullName
}