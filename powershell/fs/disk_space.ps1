function GetDiskInfo($serverName){
    Get-WMIObject -ComputerName $serverName Win32_LogicalDisk |
            ?{($_.DriveType -eq 3)}|
    #Select which attribute to show.
    select @{n='Computer' ;e={"{0:n0}" -f ($serverName)}},
    @{n='Drive' ;e={"{0:n0}" -f ($_.name)}},
    @{n='Capacity (Gb)' ;e={"{0:n2}" -f ($_.size/1gb)}},
    @{n='Free Space (Gb)';e={"{0:n2}" -f
    ($_.freespace/1gb)}},
    @{n='Percentage Free';e={"{0:n2}%" -f
    ($_.freespace/$_.size*100)}}
}