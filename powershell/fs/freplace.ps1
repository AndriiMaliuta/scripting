#File path.
$path = "c:\temp\File.txt"
#Get the file content.
Get-Content -Path $path | %{
    #Replace the token in memory, this won’t change the file content.
    $new = $_.Replace("_(TOCHANGE)","PowerShell")
}
#Set the file content.
Set-Content -Path $path -Value $new