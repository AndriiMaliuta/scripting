function ChangeUserType{
    param(
        $userID,
        [ValidateSet("Admin","User","SuperAdmin")]
        $type
    )
    ChangeType -User $userID -Type $type
}