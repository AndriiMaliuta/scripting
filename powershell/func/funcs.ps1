function writeHere($name) {
    write "Name = $name"
}

writeHere("Petro")

#You can also use the param block to define your parameters.
function MyFunction {
    param(
        $param1
        ,$param2
    )
    #Even declare where you process your logic.
    process{
        Write "This is param 1 : $param1"
        write "This is param 2 : $param2"
    }
}

#Calling MyFunction
MyFunction -param1 65 -param2 3