$path="cats.csv"
$csv=Import-CSV -Path $path -Delimiter "," -Header
"id","name","age"
$csv | %{
    $_.Name
    $_.Age
    $_.Nationality
}