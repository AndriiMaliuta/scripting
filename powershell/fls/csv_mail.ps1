<#
Routine that matter CSV file. As the CSV file has columns use the
Header-parameter to set the column headers. We also need to define the
delimiter of the CSV,
which in this case is a comma, but can be any other.
#>
$userList= Import-Csv -Path "C:\Users\rui.machado\Desktop\Users.csv"`
                                -Header "Name", "Email", "Subject"`
                                -Delimiter ","`
#SMTP Server
$smtpServer="smtp.gmail.com"
#Create a new .NET SMTP client onject (587 is the default smtp port)
$smtpClient = new-object Net.Mail.SmtpClient($smtpServer,587)
    #Create a new .NET Credential object.
    $credentials = New-Object Net.NetworkCredential
    #Set your credentials username.
    $credentials.UserName="powershellpt@gmail.com"
    #Set your credentials password.
    $credentials.Password="rpsrm@89"
#Set the smtp client credential with the one created previously.
$smtpClient.Credentials=$credentials
#Create a new .NET mail message object.
    $email = new-object Net.Mail.MailMessage
    #Initialize the from field.
        $from="powershellpt@gmail.com"
        #Initialize the mail message body.
        $message="Este é um email do powershellpt. Seja bem vindo "
#Set the from field in the mail message.
    $email.From = $from
        $email.ReplyTo = $from
        #Set the body field in the mail message.
        $email.body = $message
# foreach user in the CSV file, send the email.
$userList | %{
    #Just send to users with a defined email.
    if($_.Email -ne $null)
    {
        #Set recipient name.
        $nome=$_.Name
        #Add recipient email to the Mail message object.
        $email.To.Add($_.Email)
        #Set the subject.
        $email.subject = $_.Subject
        #This is a Google smtp server requirement.
        $smtpClient.EnableSsl=$true
        #Send the email.
        $smtpClient.Send($email)
    }
}