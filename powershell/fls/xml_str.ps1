$xmlString = @"
    <PersonList>
        <Person id='1'>
            <Name>Rui</Name>
            <Age>24</Age>
            <Address>Street A</Address>
        </Person>
            <Person id='2'>
            <Name>Peter</Name>
            <Age>45</Age>
            <Address>Street B</Address>
        </Person>
            <Person id='3'>
            <Name>Mary</Name>
            <Age>10</Age>
            <Address>Street C</Address>
        </Person>
    </PersonList>
"@
$xmlDoc = new-object System.Xml.XmlDocument
$xmlDoc.LoadXml($xmlString)
$xmlDoc.PersonList | %{
    write $_.Person.Name
}