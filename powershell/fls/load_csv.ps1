#Path to export.
$path = "c:\temp\testeExport.csv"
#Structure to export.
$csv=("Rui",24,"Portugal"),("Helder",29,"China"),("Vanessa",24,"Brasil")
#Initialize the array that will be exported.
$allRecords = @()
$csv | %{
    #Export-CSV separates columns from PSObject members, so we need to
    create one.
    $customCSV = New-Object PSObject
    #Add the name member to the PSObject
    $customCSV | Add-Member -MemberType NoteProperty -Value $_[0] -Name
    "Name"
    #Add the name member to the PSObject
    $customCSV | Add-Member -MemberType NoteProperty -Value $_[1] -Name
    "Age"
    #Add the name member to the PSObject
    $customCSV | Add-Member -MemberType NoteProperty -Value $_[2] -Name
    "Nationality"
    #Add the PSObject to the array that stores every object to export.
    $allRecords+=$customCSV
}
#Export as CSV.
$allRecords | Export-Csv -Path $path -NoTypeInformation -Delimiter ";"