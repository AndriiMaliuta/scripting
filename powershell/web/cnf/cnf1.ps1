$data = 'admin:admin'
$dataBytes = [System.Text.Encoding]::Unicode.GetBytes($data)
$encodedData = [System.Convert]::ToBase64String($dataBytes)
$authData = [System.String]::Concat('Basic ', $encodedData)
write $authData

$headers = @{
    'Authorization' = $authData
    # 'Content-Type' = 'application/json'
}

$reqUrl = 'http://localhost:7180/rest/api/space/DEV20'

# $res = Invoke-RestMethod -Uri $reqUrl -Headers $headers -Method 'GET'
$res = Invoke-WebRequest -Uri $reqUrl -Headers $headers -Method 'GET'
$res