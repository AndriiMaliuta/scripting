#Explicitly
$array1 = @(1,2,3,4,5,6,7,8,9,10)
#Implicitly
$array2 = 1,2,3,4,5,6,7,8,9,10
#Accessing with a literal index
$array1[2]
#Accessing with a variable index
$index=4
$array2[$index]