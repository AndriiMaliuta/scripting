$var1 = "PowerShell"
$var2 = $var1

#Casting to DateTime
[System.DateTime]$var3="2013-06-13"

#Casting to Xml
$var4 = [System.Xml.XmlDocument]"<xml><node>HERE</node></xml>"
$var4

$largeString= @"
Hello, this
is a PowerShell here string. You can define
multiple string lines into one single text block.
You can even use special characters like `n or expand it with another
string
    $simpleString
"@