#Import the SQLPS module
Import-Module “sqlps”

if(-not (Get-Module "sqlps")){
    Import-Module “sqlps”
}
$database="rmBlog"
$server = ".\PRI01"
#read the post name from the interactive shell.
"Please enter a post to seach`n"
$post = Read-Host
#Set the query.
$query = "SELECT p.PostTitle,
p.PostText,
p.PostDate
FROM Post p
WHERE PostTitle like '%$post%'"
$query
#Invoke the command providing as parameters all that are necessary.
Invoke-Sqlcmd -ServerInstance $server -Database $database -Query $query